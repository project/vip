<?php
namespace Drupal\vip;

/**
 * Interface VipManagerInterface.
 */
interface VipManagerInterface {
  public function addExpire($user_id, $months = 1);
}
