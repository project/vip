<?php

namespace Drupal\vip\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VIP Expire entities.
 *
 * @ingroup vip
 */
interface VIPExpireInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the VIP Expire name.
   *
   * @return string
   *   Name of the VIP Expire.
   */
  public function getName();

  /**
   * Sets the VIP Expire name.
   *
   * @param string $name
   *   The VIP Expire name.
   *
   * @return \Drupal\vip\Entity\VIPExpireInterface
   *   The called VIP Expire entity.
   */
  public function setName($name);

  /**
   * Gets the VIP Expire creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VIP Expire.
   */
  public function getCreatedTime();

  /**
   * Sets the VIP Expire creation timestamp.
   *
   * @param int $timestamp
   *   The VIP Expire creation timestamp.
   *
   * @return \Drupal\vip\Entity\VIPExpireInterface
   *   The called VIP Expire entity.
   */
  public function setCreatedTime($timestamp);

  public function addExpire($months);

  public function isExpired();

  /**
   * @return DrupalDateTime|null
   */
  public function getExpired();
}
