<?php

namespace Drupal\vip\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\TypedData\Type\DateTimeInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\user\UserInterface;

/**
 * Defines the VIP Expire entity.
 *
 * @ingroup vip
 *
 * @ContentEntityType(
 *   id = "vip_expire",
 *   label = @Translation("VIP Expire"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vip\VIPExpireListBuilder",
 *     "views_data" = "Drupal\vip\Entity\VIPExpireViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\vip\Form\VIPExpireForm",
 *       "add" = "Drupal\vip\Form\VIPExpireForm",
 *       "edit" = "Drupal\vip\Form\VIPExpireForm",
 *       "delete" = "Drupal\vip\Form\VIPExpireDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vip\VIPExpireHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\vip\VIPExpireAccessControlHandler",
 *   },
 *   base_table = "vip_expire",
 *   translatable = FALSE,
 *   admin_permission = "administer vip expire entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/people/vip_expire/{vip_expire}",
 *     "add-form" = "/admin/people/vip_expire/add",
 *     "edit-form" = "/admin/people/vip_expire/{vip_expire}/edit",
 *     "delete-form" = "/admin/people/vip_expire/{vip_expire}/delete",
 *     "collection" = "/admin/people/vip_expire",
 *   },
 *   field_ui_base_route = "vip_expire.settings"
 * )
 */
class VIPExpire extends ContentEntityBase implements VIPExpireInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  public function addExpire($months) {
    /** @var DrupalDateTime $date */
    $date = $this->get('expire')->date;
    if ($this->isExpired()) $date->setTimestamp(time());
    $date->add(new \DateInterval('P'.$months.'M'));
    $this->set('expire', $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
    return $this;
  }


  public function isExpired()
  {
    /** @var DrupalDateTime $date */
    $date = $this->get('expire')->date;
    return ($date->getTimestamp() < time());
  }

  /**
   * @return DrupalDateTime|null
   */
  public function getExpired(){
    return $this->get('expire')->date;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Belongs to'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['expire'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Expire time'))
      ->setDescription(t('The date the promotion becomes valid.'))
      ->setRequired(TRUE)
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATETIME)
      ->setDefaultValueCallback('Drupal\vip\Entity\VIPExpire::getDefaultExpireTime')
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the VIP Expire entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Default value callback for 'start_date' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return string
   *   The default value (date string).
   */
  public static function getDefaultExpireTime() {
    $timestamp = \Drupal::time()->getRequestTime();
    return gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $timestamp);
  }
}
