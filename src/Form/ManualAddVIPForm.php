<?php

namespace Drupal\vip\Form;

use Drupal\vip\Entity\VIPExpireInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\user\Entity\User;

/**
 * Class ManualAddVIPForm.
 */
class ManualAddVIPForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vip_manual_add_vip_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['months'] = [
      '#type' => 'number',
      '#title' => $this->t('时长'),
      '#description' => $this->t('要添加的时长（月）'),
      '#field_suffix' => '个月',
      '#default_value' => '1',
      '#weight' => '0',
    ];
    $user = $this->getRouteMatch()->getParameter('user');
    if ($user) {
      $user = User::load($user);
    }
    $form['user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('用户'),
      '#description' => $this->t('为他添加VIP权限'),
      '#target_type' => 'user',
      '#default_value' => $user,
      '#disabled' => true,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
      if ($form_state->getValue($key) == NULL) {
        $form_state->setErrorByName($key, '请输入' . $key);
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // 为用户添加 vip 时长
    $values = $form_state->getValues();
    /** @var VIPExpireInterface $VIPExpire */
    $VIPExpire = \Drupal::service('vip.vip_manager')->addExpire($values['user'], $values['months']);
    \Drupal::messenger()->addMessage('该用户的VIP时长添加至 ：'. $VIPExpire->getExpired()->format(DateTimeItemInterface::DATE_STORAGE_FORMAT));
  }

}
