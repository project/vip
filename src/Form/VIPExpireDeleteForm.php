<?php

namespace Drupal\vip\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting VIP Expire entities.
 *
 * @ingroup vip
 */
class VIPExpireDeleteForm extends ContentEntityDeleteForm {


}
