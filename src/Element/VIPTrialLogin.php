<?php
namespace Drupal\vip\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * @RenderElement("vip_trial_login")
 */
class VIPTrialLogin extends RenderElement {

  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'vip_trial_login',
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ]
    ];
  }

  public static function preRenderElement(array $element) {
    // 如果未登录，提示登录
    // 已登录，没权限，提示购买
    $element['#attributes'] = [
      'class' => [
        'vip_trial_login'
      ]
    ];
    if (!\Drupal::currentUser()->isAuthenticated()) {
      $current_path = \Drupal::service('path.current')->getPath();
      $result = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);

      $element['login'] = [
        '#type' => 'link',
        '#title' => t('Login'),
        '#url' => \Drupal\Core\Url::fromRoute('user.login', [
          'destination' => $result
        ])
      ];
      $element['register'] = [
        '#type' => 'link',
        '#title' => t('Register'),
        '#url' => \Drupal\Core\Url::fromRoute('user.register')
      ];
    } else {
      if (!in_array('vip', \Drupal::currentUser()->getRoles())) {
        $element['buy_vip'] = [
          '#type' => 'link',
          '#title' => t('Buy VIP'),
          '#url' => \Drupal\Core\Url::fromRoute('entity.commerce_product.canonical', [
            'commerce_product' => 1
          ])
        ];
      }
    }
    return $element;
  }
}
