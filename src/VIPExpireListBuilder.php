<?php

namespace Drupal\vip;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Defines a class to build a listing of VIP Expire entities.
 *
 * @ingroup vip
 */
class VIPExpireListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('VIP Expire ID');
    $header['name'] = $this->t('Name');
    $header['user'] = $this->t('用户');
    $header['expire'] = $this->t('失效时间');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\vip\Entity\VIPExpire $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.vip_expire.edit_form',
      ['vip_expire' => $entity->id()]
    );
    $row['user'] = Link::createFromRoute(
      $entity->getOwner()->getAccountName(),
      'entity.user.edit_form',
      ['user' => $entity->getOwnerId()]
    );
    $row['expire'] = $entity->getExpired()->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);
    return $row + parent::buildRow($entity);
  }

}
