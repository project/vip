<?php
namespace Drupal\vip;

use Drupal\vip\Entity\VIPExpire;
use Drupal\vip\Entity\VIPExpireInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\user\Entity\User;

/**
 * Class VipManager.
 */
class VipManager implements VipManagerInterface {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new VipManager object.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * @param $user_id
   * @param int $months
   * @return VIPExpireInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addExpire($user_id, $months = 1)
  {
    /** @var User $user */
    $user = User::load($user_id);

    if (!$user) {
      throw new \Exception('用户不存在');
    }

    // 查找是否已存在品牌期限
    /** @var ContentEntityStorageInterface $expireStorage */
    $expireStorage = \Drupal::entityTypeManager()->getStorage('vip_expire');
    $expire = $expireStorage->loadByProperties([
      'user_id' => $user_id
    ]);
    $expire = reset($expire);

    if (!$expire instanceof VIPExpireInterface) {
      $expire = VIPExpire::create([
        'name' => $user->getUsername() . '的VIP时长',
        'user_id' => $user_id
      ]);
      $expire->save();
    }

    // 增加期限
    $expire->addExpire($months);
    $expire->save();

    // 为用户增加VIP会员角色
    if (!$user->hasRole('vip')) {
      $user->addRole('vip');
      $user->save();
    }

    return $expire;
  }
}
