<?php

namespace Drupal\vip;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the VIP Expire entity.
 *
 * @see \Drupal\vip\Entity\VIPExpire.
 */
class VIPExpireAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\vip\Entity\VIPExpireInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view vip expire entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit vip expire entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete vip expire entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add vip expire entities');
  }


}
