<?php

namespace Drupal\vip\EventSubscriber;

use Drupal\commerce_checkout\Event\CheckoutCompletionRegisterEvent;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class DefaultSubscriber.
 */
class DefaultSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new DefaultSubscriber object.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = ['updateVIPExpire'];
    $events[CheckoutEvents::COMPLETION_REGISTER] = ['updateVIPExpire'];

    return $events;
  }

  /**
   * This method is called when the company_information_review.approve.post_transition is dispatched.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   The dispatched event.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateVIPExpire(Event $event) {
    /** @var OrderInterface $commerce_order */
    $commerce_order = null;
    if ($event instanceof CheckoutCompletionRegisterEvent) {
      $commerce_order = $event->getOrder();
    } else {
      $commerce_order = $event->getEntity();
    }

    if ($commerce_order->bundle() !== 'vip') return;

    foreach ($commerce_order->getItems() as $orderItem) {
      /** @var ProductVariationInterface $variation */
      $variation = $orderItem->getPurchasedEntity();
      $quantity = $orderItem->getQuantity();
      \Drupal::service('vip.vip_manager')->addExpire($commerce_order->getCustomerId(), $variation->field_months->value * $quantity);
    }
  }

}
